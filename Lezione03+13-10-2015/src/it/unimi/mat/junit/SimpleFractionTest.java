package it.unimi.mat.junit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleFractionTest {
	
	private SimpleFraction f1, f2;

	@Before
	public void setUp() throws Exception {
		f1 = new SimpleFraction(15, 25);
		f2 = new SimpleFraction(-27,6);
	}

	@Test
	public void testSimplify() {
		f1.simplify();
		assertEquals(3, f1.getNumerator());
		assertEquals(5, f1.getDenominator());
		
//		f2.simplify();
//		assertEquals(-9, f2.getNumerator());
//		assertEquals(2, f2.getDenominator());
		
	}

	@Test
	public void testGetDenominator() {
		int result = f1.getDenominator();
		assertTrue("getDenominator() returned " + result + " instead of 25", result==25);
		
		result = f2.getDenominator();
		assertEquals(6, result);
	}

}
