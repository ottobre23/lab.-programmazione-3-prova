package it.unimi.mat.designpatterns.flyweight;

public interface Flyweight {
	public String getCompany();
	public String getAddress();
	public String getCity();
	public String getState();
	public String getZip();
}
