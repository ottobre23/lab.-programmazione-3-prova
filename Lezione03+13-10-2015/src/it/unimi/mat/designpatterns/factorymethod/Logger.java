package it.unimi.mat.designpatterns.factorymethod;

public interface Logger {
	public void log(String msg);
}
