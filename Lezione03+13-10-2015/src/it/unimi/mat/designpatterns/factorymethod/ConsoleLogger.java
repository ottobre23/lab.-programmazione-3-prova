package it.unimi.mat.designpatterns.factorymethod;

public class ConsoleLogger implements Logger {
	public void log(String msg) {
		System.out.println(msg);

	}
}
