package it.unimi.mat.generics;

import java.util.ArrayList;
import java.util.List;

public class MyCanvas {

	public abstract class Shape {
		public abstract void draw(MyCanvas c);
	}
	
	public class Circle extends Shape {
		private int x, y, radius;
		public void draw(MyCanvas c) {
			System.out.print(c + "-" + x + "-" + y + "-" + radius);
		}
	}
	
	public class Rectangle extends Shape {
		private int x, y;
		public void draw(MyCanvas c) {
			System.out.print(c + "-" + x + "-" + y);
		}
	}
	
	public void drawAll(List<Shape> shapes) {
		for(Shape s : shapes)
			s.draw(this);
	}
	
	public void drawAll2(List<? extends Shape> shapes) {
		for(Shape s : shapes)
			s.draw(this);
	}
	
	public static void main(String args[]) {
		MyCanvas c = new MyCanvas();
		Rectangle r = c.new Rectangle();
		Circle ci = c.new Circle();
		
		List<Shape> l1 = new ArrayList<Shape>();
		l1.add(r);
		l1.add(ci);
		c.drawAll(l1);
		
		List<Circle> l2 = new ArrayList<Circle>();
		l2.add(ci);
		l2.add(ci);
		//c.drawAll(l2); //non funziona
		c.drawAll2(l2);
		
	}
	
	
	
}
