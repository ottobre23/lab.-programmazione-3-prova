package it.unimi.mat.generics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Wildcards {
	
	@SuppressWarnings("unchecked")
	static void printCollection40(Collection c) {
		Iterator i = c.iterator();
		for(int k=0; k<c.size(); k++)
			System.out.println(i.next());
	}
	
	static void printCollection(Collection<Object> c) {
		for(Object e : c)
			System.out.println(e);
	}
	
	static void printCollectionWildcard(Collection<?> c) {
		for(Object e : c)
			System.out.println(e);
	}
	
	static void writeCollection(Collection<?> c) {
		//c.add(new Object());
	}
	
	static void notBoundedWildcards(List<Exception> e) {
		for(Exception ex : e)
			ex.printStackTrace();
	}
	
	static void boundedWildcards1(List<? extends Runnable> e) { }
	
	static void boundedWildcards(List<? extends Exception> e) {
		for(Exception ex : e)
			ex.printStackTrace();
	}
	
	@SuppressWarnings({"unchecked"})
	public static void main(String args[]) {
		List l = new ArrayList();
		l.add("uno");
		l.add("due");
		l.add("tre");
		printCollection40(l);
		
		List<String> l1 = new ArrayList<String>();
		l1.add("uno");
		l1.add("due");
		l1.add("tre");
		//printCollection(l1);
		printCollectionWildcard(l1);
		writeCollection(l1);
		
		List<Object> lo = new ArrayList<Object>();
		lo.add(new Object());
		lo.add(new Object());
		printCollectionWildcard(lo);
		
		List<Runnable> lr = new ArrayList<Runnable>();
		lr.add(new Thread());
		boundedWildcards1(lr);
		
		List<Exception> le = new ArrayList<Exception>();
		le.add(new ArrayIndexOutOfBoundsException());
		le.add(new NullPointerException());
		//printCollectionWildcard(le);
		notBoundedWildcards(le);
//		
//		boundedWildcards(le);
//		
//		List<NullPointerException> ll = new ArrayList<NullPointerException>();
//		ll.add(new NullPointerException());
//		ll.add(new NullPointerException());
//		ll.add(new NullPointerException());
//		//notBoundedWildcards(ll);
//		boundedWildcards(ll);
	}
}
