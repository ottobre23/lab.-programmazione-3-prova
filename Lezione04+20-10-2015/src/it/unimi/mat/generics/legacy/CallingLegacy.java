package it.unimi.mat.generics.legacy;

import java.util.ArrayList;
import java.util.Collection;

public class CallingLegacy {

	public class Blade implements Part {
		
	}
	
	public class Guillotine implements Part {
		
	}
	
	public static void main(String[] args) {
		
		Collection<Part> c = new ArrayList<Part>();
		
		CallingLegacy l = new CallingLegacy();
		c.add(l.new Guillotine());
		c.add(l.new Blade());
		
		Inventory.addAssembly("inv", c);
		@SuppressWarnings({"unused", "unchecked"})
		Collection<Part> k = Inventory.getAssembly("inv").getParts();
		
	}
	
}
