package it.unimi.mat.generics;

import java.util.ArrayList;
import java.util.List;

public class Subtyping {

	@SuppressWarnings("unused")
	public static void main(String args[]) {
		
		List<String> ls = new ArrayList<String>();
		//List<Object> lo = ls; // non funziona!
		
		//altrimenti si potrebbe eseguire...
//		lo.add(new Object());
//		String s = ls.get(0);
	}
	
}
