package it.unimi.mat.generics;

import java.util.ArrayList;
import java.util.Collection;

public class GenericMethods {

	@SuppressWarnings("unused")
	static void fromArrayToCollectionWrong(Object[] a, Collection<?> c) {
		for(Object o : a) {
			//c.add(o);
		}
	}
	
	static <T> void fromArrayToCollection(T[] a, Collection<T> c) {
		for(T o : a)
			c.add(o);
	}
	
	static <T extends Number> void fromNumericArrayToCollection(T[] a, Collection<T> c) {
		for(T o : a)
			c.add(o);
	}
	
	public static void main(String[] args) {
		Object[] oa = new Object[100];
		Collection<Object> co = new ArrayList<Object>();
		fromArrayToCollection(oa, co);
		
		String[] sa = new String[100];
		Collection<String> cs = new ArrayList<String>();
		fromArrayToCollection(sa, cs);
		
		Integer[] ia = new Integer[100];
		Float[] fa = new Float[100];
		Number[] na = new Number[100];
		Collection<Number> cn = new ArrayList<Number>();
		fromArrayToCollection(ia, cn);
		//GenericMethods.<Number>fromArrayToCollection(ia, cn);
		fromArrayToCollection(fa, cn);
		fromArrayToCollection(na, cn);
		fromArrayToCollection(na, co);
		
		//fromArrayToCollection(na, cs);
		
		Collection<Float> cf = new ArrayList<Float>();
		fromNumericArrayToCollection(fa, cf);
		//fromNumericArrayToCollection(sa, cs);
	}
	
}
