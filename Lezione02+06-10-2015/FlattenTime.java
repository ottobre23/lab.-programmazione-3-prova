
import it.unimi.mat.serialization.PersistentTime;

import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FlattenTime {

	public static void main(String args[]) {
		PersistentTime time = new PersistentTime();
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream("qqq.dump");
			out = new ObjectOutputStream(fos);
			out.writeObject(time);
			out.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
