
package it.unimi.mat.mediator;

import it.unimi.mat.command.Command;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class UpDownShell {
	
	private final Mediator m = new Mediator(7);
	private final Command c;
	private Shell s;
	
	public UpDownShell(Display d, Command com) {
		c = com;
		s = new Shell(d);
		
		s.setLayout(new RowLayout());
		
		Text txtHowMany = new Text(s, SWT.BORDER);
		txtHowMany.setEditable(false);
		txtHowMany.setSize(20,10);
		m.registerTxtHowMany(txtHowMany);
		
		Button btnUp = new Button(s, SWT.PUSH);
		m.registerBtnUp(btnUp);
		btnUp.setText("Up");
		btnUp.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						m.up();
					}
				}
			);
		
		
		Button btnDown = new Button(s, SWT.PUSH);
		m.registerBtnDown(btnDown);
		btnDown.setText("Down");
		btnDown.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						m.down();
					}
				}
			);
		
		Button btnGo = new Button(s, SWT.PUSH);
		m.registerBtnGo(btnGo);
		btnGo.setText("Go!");
		btnGo.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						c.execute(m.getValue());
					}
				}
			);
		
		m.init();
	}
	
	public void open() {
		s.open();
	}
	
	public void pack() {
		s.pack();
	}
	
	public void setSize(int w, int h) {
		s.setSize(w, h);
	}
	
	public boolean isDisposed() {
		return s.isDisposed();
	}	

}
