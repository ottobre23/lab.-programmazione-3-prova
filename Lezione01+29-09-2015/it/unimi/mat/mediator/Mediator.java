
package it.unimi.mat.mediator;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;

public class Mediator {
	
	private Button btnUp;
	private Button btnDown;
	private Button btnGo;
	private Text txtHowMany;
	private int howMany;
	private int maxValue;
	
	public Mediator(int max) {
		maxValue = max;
	}

	public void registerBtnUp(Button b) {
		btnUp = b;
	}
	
	public void registerBtnDown(Button b) {
		btnDown = b;
	}
	
	public void registerBtnGo(Button b) {
		btnGo = b;
	}
	
	public void registerTxtHowMany(Text t) {
		txtHowMany = t;
	}
	
	public void init() {
		btnUp.setEnabled(true);
		btnDown.setEnabled(false);
		btnGo.setEnabled(true);
		howMany = 0;
		txtHowMany.setText("" + howMany);
		
	}
	
	public void up() {
		if(howMany < maxValue) {
			howMany++;
			txtHowMany.setText("" + howMany);
			btnDown.setEnabled(true);
			if(howMany == maxValue)
				btnUp.setEnabled(false);
		}
	}
	
	public void down() {
		if(howMany > 0) {
			howMany--;
			txtHowMany.setText("" + howMany);
			btnUp.setEnabled(true);
			if(howMany == 0)
				btnDown.setEnabled(false);
		}
	}
	
	public int getValue() {
		return howMany;
	}
	
}
