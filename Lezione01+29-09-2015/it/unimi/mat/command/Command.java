package it.unimi.mat.command;

public interface Command {
	public void execute(int n);

}
