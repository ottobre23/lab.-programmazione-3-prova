
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class ExitSelectionAdapter extends SelectionAdapter {

	public void widgetSelected(SelectionEvent e) {
		e.display.dispose();
		System.exit(1);
	}	
	
}
