import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.*;

import java.util.Date;

public class TestControls {
	
	public static void addButton(Shell s) {
		Button b1 = new Button(s, SWT.PUSH);
		b1.setText("OK");
		b1.setBounds(0, 0, 100, 30);
		
		b1.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					System.out.println(e);
				}
			}
		);
		
		Button b2 = new Button(s, SWT.CHECK);
		b2.setText("check");
		b2.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					System.out.println(((Button)e.widget).getSelection());
				}
			}
		);
		
		b2.setBounds(0, 40, 100, 30);
		Button b3 = new Button(s, SWT.RADIO);
		b3.setText("radio");
		
		
		b3.setBounds(0, 80, 100, 30);
		Button b4 = new Button(s, SWT.TOGGLE);
		b4.setText("toggle");
		b4.setBounds(0, 140, 100, 30);
	}
	
	public static void addText(Shell s) {
		Text t1 = new Text(s, SWT.BORDER | SWT.V_SCROLL);
		t1.setText("");
		t1.setBounds(0, 0, 100, 40);
		
		Text t2 = new Text(s, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		t2.setText("prima riga\nseconda riga");
		t2.setBounds(40, 40, 100, 200);
		t1.setTextLimit(10);
		
		t2.setEditable(false);
		
		t2.addKeyListener(
				new KeyListener() {
					public void keyPressed(KeyEvent e) {
						String s = "";
						switch(e.character) {
						case 0:			s += " '\\0'"; break;
						case SWT.BS:	s += " '\\b'"; break;
						case SWT.CR:	s += " '\\r'"; break;
						case SWT.DEL:	s += " DEL"; break;
						case SWT.ESC:	s += " ESC"; break;
						case SWT.LF:	s += " '\\n'"; break;
						default:		s += " '" + e.character + "'";
						}
						System.out.println(s);
					}
					public void keyReleased(KeyEvent e) {
						if(e.stateMask == SWT.CTRL && e.keyCode != SWT.CTRL)
							System.out.println("Command can execute here");
					}
				}
		);
		
		t1.addModifyListener(
			new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					System.out.println("Modificato al tempo " + 
						new Date() // e.time
					);
				}
			}
		);
		
		t1.addVerifyListener(
			new VerifyListener() {
				public void verifyText(VerifyEvent e) {
					if(e.text.contains("*"))   {
						System.out.println("Carattere non utilizzabile.");
						e.doit = false;
					}
				}
			}
		);
	}
	
	public static void addList(Shell s) {
		List l = new List(s, SWT.MULTI | SWT.V_SCROLL);
		l.setItems(new String[] {"uno", "due", "tre"});
		l.add("quattro");
		l.setBounds(0, 0, 100, 200);
		
		l.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					for(String s : ((List)e.widget).getSelection())
						System.out.print(s + " ");
					System.out.println();
				}
			}
		);
	}
	
	public static void addListeners(Shell s) {
		s.addMouseListener(
			new MouseAdapter() {
				public void mouseDown(MouseEvent e) {
					if(e.stateMask != 0) {
						System.out.print("(");
						if((e.stateMask & SWT.CTRL) != 0) System.out.print(" CTRL ");
						if((e.stateMask & SWT.ALT) != 0) System.out.print(" ALT ");
						if((e.stateMask & SWT.SHIFT) != 0) System.out.print(" SHIFT ");
						System.out.print(") ");
					}
					System.out.println("pulsante " + e.button + " premuto a (" + e.x + "," + e.y +")");
				}
				public void mouseUp(MouseEvent e) {
					System.out.println("pulsante " + e.button + " rilasciato a (" + e.x + "," + e.y +")");
				}
			}
		);
		
		s.addMouseMoveListener(
			new MouseMoveListener() {
				public void mouseMove(MouseEvent e) {
					System.out.println(" mouse in (" + e.x + "," + e.y +")");
				}
			}
		);
		
		s.addMouseTrackListener(
			new MouseTrackAdapter() {
				public void mouseEnter(MouseEvent e) {
					System.out.println("mouse entrato");
				}
				public void mouseExit(MouseEvent e) {
					System.out.println("mouse uscito");
				}
			}
		);
		
	}
	
	public static void addCombo(Shell s) {
		Combo c = new Combo(s, SWT.DROP_DOWN);
		c.setItems(new String[] {"uno", "due", "tre", "quattro"});
		c.setBounds(0, 0, 100, 200);
		
		c.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					System.out.println(((Combo)e.widget).getText());
				}
				
				public void widgetDefaultSelected(SelectionEvent e) {
					System.out.println("(default)" + ((Combo)e.widget).getText());
				}
			}
		);
	}
	
	public static void addGroup(Shell s) {
		Group g = new Group(s, SWT.BORDER);
		for(int i=0; i<6; i++) {
			Button b = new Button(g, SWT.RADIO);
			b.setText("scelta numero " + i);
			b.setBounds(0, 20*(i+1), 300, 10);
			b.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						System.out.println(((Button)e.widget).getText());
					}
				}
			);
		}
		g.setText("Qui puoi fare una scelta");
		g.setBounds(0, 0, 200, 150);
	}
	
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		
		shell.setSize(100, 500);
		shell.open();
		
		
		addButton(shell);
//		addText(shell);
//		addList(shell);
//		addListeners(shell);
//		addCombo(shell);
//		addGroup(shell);
		shell.pack();
		
		while(!shell.isDisposed()) {
			if(!display.readAndDispatch()) 
				display.sleep();
		}
		
		display.dispose();
	}
}